import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import classes.AirConditioning;
import classes.Door;
import classes.Equipment;
import classes.Fridge;
import classes.Room;
import classes.Television;
import classes.Furniture;

import java.io.*;
import java.awt.GraphicsEnvironment;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class main {
  static List<Room> rooms = new ArrayList<Room>();
  static List<Room> house = new ArrayList<Room>();
  static List<Room> roomPair1 = new ArrayList<Room>();
  static List<Room> roomPair2 = new ArrayList<Room>();
  static List<Room> roomPair3 = new ArrayList<Room>();
  static List<Room> roomPair4 = new ArrayList<Room>();
  static List<Room> roomPair5 = new ArrayList<Room>();
  static List<Room> roomPair6 = new ArrayList<Room>();
  static List<Door> doors = new ArrayList<Door>();
  static List<Door> ChambreDoors = new ArrayList<Door>();
  static List<Door> SalonDoors = new ArrayList<Door>();
  static List<Door> BuanderieDoors = new ArrayList<Door>();
  static List<Door> CouloirDoors = new ArrayList<Door>();
  static List<Door> EntreeDoors = new ArrayList<Door>();
  static List<Door> CuisineDoors = new ArrayList<Door>();
  static List<Door> SalleDeBainDoors = new ArrayList<Door>();
  static List<Equipment> equipments = new ArrayList<Equipment>();
  static List<Equipment> ChambreEquipments = new ArrayList<Equipment>();
  static List<Equipment> SalonEquipments = new ArrayList<Equipment>();
  static List<Equipment> CuisineEquipments = new ArrayList<Equipment>();
  static List<Equipment> BuanderieEquipments = new ArrayList<Equipment>();
  static List<Equipment> CouloirEquipments = new ArrayList<Equipment>();
  static List<Equipment> EntreeEquipments = new ArrayList<Equipment>();
  static List<Equipment> SalleDeBainEquipments = new ArrayList<Equipment>();
  static List<String> food = new ArrayList<String>();

  // Heure de départ
  static int heure = 8;
  static boolean cycle;

  static Scanner scanner = new Scanner(System.in);
  static int i = 0;

  // Initialisation des pièces
  static Room Chambre = new Room("Chambre");
  static Room Salon = new Room("Salon");
  static Room Buanderie = new Room("Buanderie");
  static Room Couloir = new Room("Couloir");
  static Room Entree = new Room("Entree");
  static Room Cuisine = new Room("Cuisine");
  static Room SalleDeBain = new Room("Salle de bain");
  static Room current = Entree;

  static Equipment tv = new Television(" 'Et l'equipe de France est championne du monde ! ...' ", "Television");
  static Equipment tvChambre = new Television(
      " 'Toutes ces croyances a la noix et ces armes demodees, ça ne vaut pas un bon pistolaser au cote ! ...' ",
      "Television");
  static Equipment frigo = new Fridge(food, "Refrigirateur");
  static Equipment meuble = new Furniture("'Ce meuble en bois d'acajou est tres jolie !'", "Meuble");
  static Equipment bureau = new Furniture("'Je dervais nettoyer un peu ce bureau...'", "Bureau");
  static Equipment placard = new Furniture("'hmmm, il ne reste plus beaucoup de gouters, il faut penser a faire les courses !'", "Placard");
  static Equipment chevet = new Furniture("'Ah c'etait donc la que se trouvait mes lunettes !'", "Table de chevet");
  static Equipment table = new Furniture("*Il y a une tache de vin du a la soiree d'hier...*", "Table");
  static Equipment pharmacie = new Furniture("'Evidemment, la securite avant tout !'", "Pharmacie");
  static Equipment baignoire = new Furniture("*La baignoire a ete souille par la soiree...*", "Baignoire");
  static Equipment clim = new AirConditioning("Clim");


  static void Infos() {
    String light = current.getLight() ? "allumee" : "eteinte";
    

    System.out.println("\n---- Infos ----");
    System.out.println("Vous etes dans : " + current.getName());
    System.out.println("La lumiere est " + light);
    VerifieCycle();
    String window = current.getWindow() ? "ouverte" : "fermee";
    System.out.println("La fenetre est " + window);
    System.out.println("Il est " + heure + "h.");
    
    System.out.println("---------------\n");
  }

  static void Menu() {
    Infos();
    i = 1;
    rooms.clear();
    doors.clear();

    for (Door door : current.getDoors()) {
      for (Room r : door.getTransition()) {
        if (!r.equals(current)) {
          rooms.add(r);
          doors.add(door);
        }
      }
      i++;
    }

    String light = current.getLight() ? "Eteindre" : "Allumer";
    String window = current.getWindow() ? "Fermer" : "Ouvrir";

    System.out.println("\n---- Menu ----\n");

    System.out.println("(d) Deplacement");
    System.out.println("(p) Liste de Porte(s)");
    System.out.println("(l) " + light + " la lumiere");
    System.out.println("(f) " + window + " la fenetre");
    System.out.println("(e) Liste d'equipement(s)");
    System.out.println("\n(q) Quitter\n");

    System.out.print("Votre choix : ");
    String line = scanner.nextLine();

    switch (line) {
      case "d":
        Move();
        break;
      case "p":
        DoorList();
        break;
      case "l":
        Light();
        break;
      case "f":
        Window();
        break;
      case "e":
        Equipment();
        break;
      case "q":
        scanner.close();
        System.exit(0);
        break;
      default:
        System.out.print("Choix invalide");
        Menu();
        break;
    }
  }

  static void Move() {
    Infos();
    System.out.println("Dans quelle piece voulez-vous entrer ?\n");
    i = 1;
    rooms.clear();
    doors.clear();

    for (Door door : current.getDoors()) {
      for (Room r : door.getTransition()) {
        if (!r.equals(current)) {
          System.out.println(i + ". : " + r.getName());
          rooms.add(r);
          doors.add(door);
        }
      }
      i++;
    }

    System.out.println("(r) Retour\n");
    System.out.print("Votre choix : ");
    String line = scanner.nextLine();

    if (line.equals("r")) {
      Menu();
    }
    else if(Integer.parseInt(line) > 0 && Integer.parseInt(line) <= doors.size()) {
      // Porte ouverte
      if (doors.get(Integer.parseInt(line) - 1).getState()) {
        current = rooms.get(Integer.parseInt(line) - 1);
        System.out.println("\nVous etes entre dans : " + current.getName());
        heure += 5;
        heure %= 24;
      }
      // Porte fermée
      else {
        System.out.println("\nLa porte est fermee !");
        System.out.println("Voulez vous l'ouvrir ? (y) ou (n), Annuler (a)\n");
        System.out.print("Votre choix : ");
        String choice = scanner.nextLine();

        // Ouvrir porte
        if (choice.equals("y")) {
          doors.get(Integer.parseInt(line) - 1).setState(true);
          current = rooms.get(Integer.parseInt(line) - 1);
          System.out.println("\nVous etes dans : " + current.getName());
          heure += 5;
          heure %= 24;
        }
        // Annuler
        else if (choice.equals("a")) {
          Move();
        }
        // Laisser porte fermer
        else {
          System.out
              .println("La porte pour acceder a " + rooms.get(Integer.parseInt(line) - 1).getName() + " reste fermee.");
          System.out.println("\nVous etes dans : " + current.getName());
        }
      }
    }
    else {
      System.out.println("Commande invalide");
    }
    Move();
  }

  static void DoorList() {
    Infos();
    System.out.println("\nQuelle porte voulez vous Ouvrir/Fermer ?\n");
    i = 1;
    rooms.clear();
    doors.clear();

    for (Door door : current.getDoors()) {
      for (Room r : door.getTransition()) {
        if (!r.equals(current)) {
          String state;
          state = door.getState() ? "Ouverte" : "Fermee";
          System.out.println(i + ". : " + r.getName() + " (" + state + ")");
          rooms.add(r);
          doors.add(door);
        }
      }
      i++;
    }
    System.out.println("(r) Retour\n");
    System.out.print("Votre choix : ");
    String choice = scanner.nextLine();

    // Retour
    if (choice.equals("r")) {
      Menu();
    } else {
      doors.get(Integer.parseInt(choice) - 1).switchState();
      System.out.println("La porte pour " + rooms.get(Integer.parseInt(choice) - 1).getName() + " a change d'etat\n");
      heure += 5;
      heure %= 24;
      DoorList();
    }
  }

  static void Light() {
    current.SwitchLight();
    heure += 5;
    heure %= 24;
    Menu();
  }

  static void Window() {
    current.SwitchWindow();
    heure += 5;
    heure %= 24;
    Menu();
  }

  static void Equipment() {
    Infos();
    System.out.println("Liste d'equipement de la piece : " + current.getName() + "\n");
    i = 1;
    equipments.clear();

    for (Equipment e : current.getEquipments()) {
      if(e.getClass().getName() == "classes.AirConditioning"){
        String state = ((AirConditioning)e).getState() ? "Allumee" : "Eteinte";
        System.out.println(i + ". : " + e.getName() + " (" + state +")");
      }
      else System.out.println(i + ". : " + e.getName());
      
      equipments.add(e);
      i++;
    }

    if (equipments.size() == 0) {
      System.out.println("Aucun equipement dans cette piece !\n");
    }

    System.out.println("(r) Retour\n");
    System.out.print("Votre choix : ");
    String choice = scanner.nextLine();

    // Retour
    if (choice.equals("r")) {
      Menu();
    } else {
      if (equipments.size() > 0) {
        switch (equipments.get(Integer.parseInt(choice) - 1).getClass().getName()) {
          case "classes.Television":
            System.out.println(equipments.get(Integer.parseInt(choice) - 1).getName() + " : " + equipments.get(Integer.parseInt(choice) - 1).getInteraction());
            heure += 5;
            heure %= 24;
            break;
          case "classes.Fridge":
            System.out.println("Le " + equipments.get(Integer.parseInt(choice) - 1).getName() + " contient : ");
            for (String f : ((Fridge) equipments.get(Integer.parseInt(choice) - 1)).getFood()) {
              System.out.println(f);
            }
            heure += 5;
            heure %= 24;
            break;
          case "classes.Furniture":
            System.out.println(equipments.get(Integer.parseInt(choice) - 1).getName() + " : " + ((Furniture)equipments.get(Integer.parseInt(choice) - 1)).getContent());
            heure += 5;
            heure %= 24;
            break;
          case "classes.AirConditioning":
            String state = ((AirConditioning)equipments.get(Integer.parseInt(choice) - 1)).getState() ? "Allumee" : "Eteinte";
            System.out.println("\nLa clim est "+ state + " !");
            state = ((AirConditioning)equipments.get(Integer.parseInt(choice) - 1)).getState() ? "eteinre" : "allumer";
            System.out.println("Voulez vous l'"+ state +"? (y) ou (n), Annuler (a)\n");
            System.out.print("Votre choix : ");
            String selection = scanner.nextLine();
    
            // Allumer clim
            if (selection.equals("y")) {
              ((AirConditioning)equipments.get(Integer.parseInt(choice) - 1)).switchState();
              heure += 5;
              heure %= 24;
            }
            // Annuler
            else if (choice.equals("a")) {
              Equipment();
            }
            // Laisser clim eteinte
            else {
              System.out.println("La clim reste eteinte.");
            }
            break;
          default:
            break;
        }

        System.out.println("\nAppuyez sur entrer pour revenir a la liste d'equipements");
        scanner.nextLine();

      }
      Equipment();
    }
  }

  static void VerifieCycle() {
    if (heure >= 6 && heure <= 19) {
      cycle = true;
      System.out.println("Il fait jour");
    } else{
      System.out.println("Il fait nuit");
      cycle = false;
      for (Room r : house) {
        r.setWindow(cycle);
      }
    }
  }

  public static void main(String[] args) {

    // Liste des pièces de la maison \\
    house.add(Chambre);
    house.add(Salon);
    house.add(Buanderie);
    house.add(Couloir);
    house.add(Entree);
    house.add(Cuisine);
    house.add(SalleDeBain);


    // Initialisation des portes \\
    roomPair1.add(Couloir);
    roomPair1.add(Buanderie);
    Door d1 = new Door(roomPair1);

    roomPair2.add(Couloir);
    roomPair2.add(Salon);
    Door d2 = new Door(roomPair2);

    roomPair3.add(Couloir);
    roomPair3.add(Chambre);
    Door d3 = new Door(roomPair3);

    roomPair4.add(Chambre);
    roomPair4.add(SalleDeBain);
    Door d4 = new Door(roomPair4);

    roomPair5.add(Entree);
    roomPair5.add(Salon);
    Door d5 = new Door(roomPair5);

    roomPair6.add(Cuisine);
    roomPair6.add(Salon);
    Door d6 = new Door(roomPair6);

    // Attribution des portes \\
    ChambreDoors.add(d4);
    ChambreDoors.add(d3);
    Chambre.setDoors(ChambreDoors);

    SalonDoors.add(d5);
    SalonDoors.add(d6);
    SalonDoors.add(d2);
    Salon.setDoors(SalonDoors);

    BuanderieDoors.add(d1);
    Buanderie.setDoors(BuanderieDoors);

    CouloirDoors.add(d1);
    CouloirDoors.add(d2);
    CouloirDoors.add(d3);
    Couloir.setDoors(CouloirDoors);

    EntreeDoors.add(d5);
    Entree.setDoors(EntreeDoors);

    CuisineDoors.add(d6);
    Cuisine.setDoors(CuisineDoors);

    SalleDeBainDoors.add(d4);
    SalleDeBain.setDoors(SalleDeBainDoors);

    // Attribution des équipements \\
    SalonEquipments.add(tv);
    SalonEquipments.add(meuble);
    SalonEquipments.add(clim);
    Salon.setEquipments(SalonEquipments);

    food.add("Lait");
    food.add("Coca");
    food.add("Fruits");
    food.add("Fromage");
    food.add("Yaourt");
    CuisineEquipments.add(frigo);
    CuisineEquipments.add(table);
    CuisineEquipments.add(clim);
    Cuisine.setEquipments(CuisineEquipments);

    ChambreEquipments.add(chevet);
    ChambreEquipments.add(bureau);
    ChambreEquipments.add(clim);
    Chambre.setEquipments(ChambreEquipments);

    BuanderieEquipments.add(placard);
    Buanderie.setEquipments(BuanderieEquipments);

    EntreeEquipments.add(clim);
    Entree.setEquipments(EntreeEquipments);

    CouloirEquipments.add(clim);
    Couloir.setEquipments(CouloirEquipments);

    SalleDeBainEquipments.add(clim);
    SalleDeBain.setEquipments(SalleDeBainEquipments);

    Console console = System.console();
    String path = main.class.getProtectionDomain().getCodeSource().getLocation().toString().substring(6);
    Path p = Paths.get(path);
    String filename = p.getFileName().toString();
    
    if(console == null && !GraphicsEnvironment.isHeadless()){
      System.out.println(filename);
      try {
        Runtime.getRuntime().exec(new String[]{"cmd","/c","start","cmd","/k","java -jar \"" + "./"+ filename + "\""});
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    Menu();
  }

}
