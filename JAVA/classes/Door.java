package classes;

import java.util.ArrayList;
import java.util.List;

public class Door {
    List<Room> transition = new ArrayList<Room>();
    Boolean state = false;

    public Door(List<Room> transition) {
        this.transition = transition;
    }

    public void setTransition(List<Room> transition) {
        this.transition = transition;
    }

    public Boolean isState() {
        return this.state;
    }

    public Boolean getState() {
        return this.state;
    }

    public void switchState() {
        this.state = !this.state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public List<Room> getTransition(){
        return this.transition;
    }


}


