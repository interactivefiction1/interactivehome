package classes;

public class Television extends Equipment{
    String interaction;

    public Television(String interaction, String name) {
        super(name);
        this.interaction = interaction;
    }

    public String getInteraction() {
        return this.interaction;
    }

    public void setInteraction(String interaction) {
        this.interaction = interaction;
    }

}
