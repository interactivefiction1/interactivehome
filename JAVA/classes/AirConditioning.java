package classes;

public class AirConditioning extends Equipment{
    boolean state = false;

    public AirConditioning(String name) {
        super(name);
    }

    public boolean isState() {
        return this.state;
    }

    public boolean getState() {
        return this.state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public void switchState() {
        this.state = !this.state;
    }

}
