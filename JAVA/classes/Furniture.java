package classes;

public class Furniture extends Equipment {
    String content;

    public Furniture(String content, String name) {
        super(name);
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
}
