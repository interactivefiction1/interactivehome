package classes;

public class Equipment {
    String name;
    String interactionType;

    public Equipment(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInteraction() {
        return this.interactionType;
    }

    public void setInteraction(String interaction) {
        this.interactionType = interaction;
    }

}
