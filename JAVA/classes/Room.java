package classes;

import java.util.ArrayList;
import java.util.List;

public class Room {
    String name;
    Boolean light = false;
    Boolean window = true;
    private List<Door> doors = new ArrayList<Door>();
    List<Equipment> equipments = new ArrayList<Equipment>();

    public Room(String name) {
        this.name = name;
    }

    public List<Equipment> getEquipments() {
        return this.equipments;
    }

    public void setEquipments(List<Equipment> equipments) {
        this.equipments = equipments;
    }

    public List<Door> getDoors() {
        return this.doors;
    }

    public void setDoors(List<Door> doors) {
        this.doors = doors;
    } 

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
 
    public Boolean isLight() {
        return this.light;
    }

    public Boolean getLight() {
        return this.light;
    }

    public void setLight(Boolean light) {
        this.light = light;
    }

    public void SwitchLight() {
        this.light = !this.light;
    } 

    public Boolean isWindow() {
        return this.window;
    }

    public Boolean getWindow() {
        return this.window;
    }

    public void setWindow(Boolean window) {
        this.window = window;
    }

    public void SwitchWindow() {
        this.window = !this.window;
    } 

 }