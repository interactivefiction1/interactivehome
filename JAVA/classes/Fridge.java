package classes;

import java.util.ArrayList;
import java.util.List;

public class Fridge extends Equipment{
    List<String> food = new ArrayList<String>();

    public Fridge(List<String> food, String name) {
        super(name);
        this.food = food;
    }

    public List<String> getFood() {
        return this.food;
    }

    public void setFood(List<String> food) {
        this.food = food;
    }

}
