# InteractiveHome



## Getting started
Créer un environnement représentant une maison avec plusieurs pièces

- Permettre le déplacement entre les pièces
    - une navigation d’une pièce à l’autre est contrainte par la géographie : vous ne pouvez passer d’une pièce à l’autre que si il existe une porte ouverte
    <br>
- Une pièce contient une ou plusieurs portes
    - Possibilité d’ouvrir ou fermer
    <br>
- Une pièce contient une ou plusieurs fenêtres
    - Possibilité d’ouvrir ou fermer le volet associé

## Lancement du programme
[Lancement de l'exécutable](interactivehome.jar)

## User Story
Rendez-vous sur [GoogleDoc](https://docs.google.com/document/d/1Bokv59wGK-pSYa22y-Jll1SSZTvNrnsZJGemzC9ftzk/edit) !

## Maven version + CI
La version maven du projet avec un simple CI se trouve [ici](https://gitlab.com/interactivefiction1/interactivehome/-/tree/master)!

## Schéma de la maison
![Schema](SchemaMaion.png)

## Présentation des classes

* [main.java](JAVA/main.java)
    * Fichier principal du projet, permettant le lancement du programme
    * Contient les traitements des objets
    <br>

* [Door.java](JAVA/classes/Door.java)
    * L'objet Door permet de gérer les portes de la maison
    * Il contient 2 attributs
        * L'attribut transition permet de stocker la liste de pièces que permet d'accéder la porte
            * ```List<Room> transition = new ArrayList<Room>();```
        * L'attibut state est un boolean qui gère l'état de la porte. Ouvert = true, Fermée = false
            * ```Boolean state = false;```
    <br>

* [Equipment.java](JAVA/classes/Equipment.java)
    * L'objet Equipment permet de gérer les différents équipements de la maison
    * Il contient 1 attribut
        * L'attribut name qui correspond au nom de notre équipement
            * ```String name;```
    <br>

* [AirConditioning.java](JAVA/classes/AirConditioning.java)
    * L'objet AirConditioning représente l'équipement Climatiseur
    * Il contient 1 attribut
        * L'attribut state qui correspond à l'état du climatiseur. Allumée = true, Éteinte = false
            * ```boolean state = false;```
    <br>

* [Fridge.java](JAVA/classes/Fridge.java)
    * L'objet Fridge représente l'équipement Réfrigirateur
    * Il contient 1 attribut
        * L'attribut food est une list qui stock les aliments de notre réfrigirateur
            * ```List<String> food = new ArrayList<String>();```
    <br>

* [Furniture.java](JAVA/classes/Furniture.java)
    * L'objet Furniture représente l'équipement Meuble
    * Il contient 1 attribut
        * L'attribut content représente la phrase qui sera affiché lors de l'intéraction avec l'utilisateur
            * ```String content;```
    <br>

* [Television.java](JAVA/classes/Television.java)
    * L'objet Television représente l'équipement Télévision
    * Il contient 1 attribut
        * L'attribut interaction représente la phrase qui sera affiché lors de l'intéraction avec l'utilisateur
            * ```String interaction;```
    <br>

* [Room.java](JAVA/classes/Room.java)
    * L'objet Room représente une pièce
    * Il contient 5 attributs
        * L'attribut name représente le nom de la pièce
            * ```String name;```
        * L'attribut light permet de gérer la lumière de la pièce. Allumée = true, Éteinte = false
            * ```Boolean light = false;```
        * L'attribut window permet de gérer l'état de la fenêtre de la pièce
            * ```Boolean window = true;```
        * L'attribut doors est une liste de portes que contient la pièce
            * ```private List<Door> doors = new ArrayList<Door>();```
        * L'attribut equipments est la liste des équipements que contient la pièce
            * ```List<Equipment> equipments = new ArrayList<Equipment>();```
    <br>


## Synthèse

Nous avons donc implémenté toutes les fonctions principales nécessaires pour le projet.
En ce qui concerne nos User story, nous avons dû en abandonner une ("ajouter/retirer des équipements dans une pièce en mode Admin").





